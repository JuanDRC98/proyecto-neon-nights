﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ani : MonoBehaviour
{
    [SerializeField] private Animator ani;
    
    public AudioSource AHappyDay;
   
    //public void DisplayCharacter(bool hide)
    //{
    //chibi.SetBool("Sale", hide);
    //}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Funciona"))
        {
            
            ani.SetBool("Sale", true);
            AHappyDay.Play();
        }
        
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Funciona"))
        {
            ani.SetBool("Sale", false);
            AHappyDay.Stop();
        }
        
    }

    public void clickanim()
    {
        ani.SetBool("selected", true);
    }

    public void clickanim2()
    {
        ani.SetBool("selected", false);
    }

    public void clicked()
    {
        ani.SetBool("click", true);
    }
}
