﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movie : MonoBehaviour

{

    public UnityEngine.Video.VideoClip videoClip;
    public UnityEngine.Video.VideoPlayer videoPlayer;
    public bool startPlaying;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnStartGame.AddListener(StartVideo);
    }

    // Update is called once per frame
    private void StartVideo()
    {

        startPlaying = true;
        videoPlayer.clip = videoClip;
        videoPlayer.Play();

    }
}

