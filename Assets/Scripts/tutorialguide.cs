﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tutorialguide : MonoBehaviour
{
    [System.Serializable]
    public struct GuideData
    {
      public  GameObject prefabImage;
      public  float WaitTime;
      public float InScreenTime;
    }
    public bool startPlaying;

    public GuideData[] tutorial;

    private string currenttext = "";

    int index = 0;
    private Coroutine coroutine;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnStartGame.AddListener(StartGuide);
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator Tutorialslider(float seconds)
    {
        GameManager.instance.SetUpdateEscapeButton(false);
        for(int i=0; i < tutorial.Length; i++)
        {
            while (GameManager.IsGamePaused)
            {
                yield return null;
            }
            tutorial[i].prefabImage.SetActive(true);
            yield return new WaitForSeconds(tutorial[i].InScreenTime);
            while (GameManager.IsGamePaused)
            {
                yield return null;
            }
            tutorial[i].prefabImage.SetActive(false);
            yield return new WaitForSeconds(tutorial[i].WaitTime);
        }
        GameManager.instance.SetUpdateEscapeButton(true);
    }

    void StartGuide()
    {
        StartCoroutine(Tutorialslider(5));
        coroutine = StartCoroutine(Tutorialslider(5));
    }
}






