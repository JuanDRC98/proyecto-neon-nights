﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreTable : MonoBehaviour
{
    [SerializeField] private Transform entryContainer;
    [SerializeField] private Transform entryTemplate;
    [SerializeField] private Transform posText;
    [SerializeField] private Transform scoretext;
    private List<Transform> highscoreEntryTransformList;

    private void Awake()
    {

        entryTemplate.gameObject.SetActive(false);

        AddHighscoreEntry(10000);

        /*highscoreEntryList = new List<HighscoreEntry>()
        {
            new HighscoreEntry { score = 523457 },
            new HighscoreEntry { score = 55786757 },
            new HighscoreEntry { score = 5686857 },
            new HighscoreEntry { score = 7 },
            new HighscoreEntry { score = 643457 },
            new HighscoreEntry { score = 457 },
            new HighscoreEntry { score = 5564367 },
            new HighscoreEntry { score = 222457 },
            new HighscoreEntry { score = 57 },
            new HighscoreEntry { score = 9657 },
            new HighscoreEntry { score = 657 }
        };*/
        string jsonString = PlayerPrefs.GetString("highscoreTable");

        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if(highscores == null)
        {
            highscores = new Highscores();
        }

        //lista hecha por puntuación
        for (int i= 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for(int j = 1 + i; j < highscores.highscoreEntryList.Count;j++)
            {
                if(highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    //ordenar valores
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        /*
        highscoreEntryTransformList = new List<Transform>();
        foreach (HighscoreEntry highscoreEntry in highscoreEntryList)
        {
            CreateHighscoreEntryTransform(highscoreEntry,entryContainer, highscoreEntryTransformList);
        }
        Highscores highscores = new Highscores { highscoreEntryList = highscoreEntryList };
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
        Debug.Log(PlayerPrefs.GetString("highscoreTable"));
        */
     }    
    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container,List<Transform> transformList)
    {
        float templateHeight = 30f;

        {
            Transform entryTransform = Instantiate(entryTemplate, container);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
            entryTransform.gameObject.SetActive(true);

            int rank = transformList.Count + 1;
            string rankString;
            switch (rank)
            {
                default:rankString = rank + "TH"; break;
                case 1: rankString = "1ST"; break;
                case 2: rankString = "2ND"; break;
                case 3: rankString = "3RD"; break;
            }

            posText.GetComponent<Text>().text = rankString;

            int score = highscoreEntry.score ;

            scoretext.GetComponent<Text>().text = score.ToString();

            transformList.Add(entryTransform);
        }
    }

    private void AddHighscoreEntry(int score)
    {
        // crea una entry de la tabla de puntuación
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score };

        // La carga de datos de la tabla de puntuación
        string jsonString = PlayerPrefs.GetString("highscoreTable");

        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        // Añade nueva entry a la tabla de puntuación
        highscores.highscoreEntryList.Add(highscoreEntry);
        
        //Guardado actualizado de la tabla de puntuación 
        string json = JsonUtility.ToJson(highscores);

        PlayerPrefs.SetString("highscoreTable", json);

        PlayerPrefs.Save();
    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    [System.Serializable]
    private class HighscoreEntry
    {
        public int score;
    }
}

    


