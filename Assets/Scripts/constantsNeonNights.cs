﻿using UnityEngine.Events;

public enum datasettings
{
  VolumeSlider,
}
[System.Serializable]
public struct ResourceCost
{
    public int score;
    public datasettings type;

}
public static class GameConstants
{
    public static readonly float UpdateRate = 60.0f;
    public static readonly string NeonNightSKey = "CurrentSlot"; //currentslot name
}


//Events
[System.Serializable]
public class ResourceEvent : UnityEvent<float>
{

}