﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightactivator: MonoBehaviour
{
    public Light myLight;

    // Start is called before the first frame update
    void Start()
    {
        myLight.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            myLight.enabled = true;
        }
        
    }
}
