﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menupausee : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

    public Image fadeImage;

    public AudioSource theMusic;

    public Image[] countDownImages;

    float waitTime = 1.0f;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
//        pauseMenuUI.SetActive(false);
        StartCoroutine(Fade());

    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        tempo.update = 0;
        GameIsPaused = true;
        musicpaused();
    }

    public void LoadMenu()
    {
        GameIsPaused = true;
        musicpaused();
        Debug.Log("Loading menu...");
        tempo.update = 1;
        StartCoroutine(LoadYourAsyncScene("MenuSelection"));


    }
    void musicpaused()
    {

        theMusic.Pause();

    }
    void musicunpaused()
    {

        theMusic.UnPause();

    }


    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;



        float fadeIntensity = 0;

        Color col;
        fadeImage.gameObject.SetActive(true);
        while (fadeIntensity < 1)
        {
            fadeIntensity += 0.5f * Time.deltaTime;

            col = fadeImage.color;
            col.a = fadeIntensity;

            fadeImage.color = col;

            yield return null;
        }

        col = fadeImage.color;
        col.a = fadeIntensity;

        fadeImage.color = col;
        yield return new WaitForSeconds(0.3f);
        asyncLoad.allowSceneActivation = true;
    }
    IEnumerator Fade()
    {
        countDownImages[0].gameObject.SetActive(true);

        for (int i = 0; i < 3; i++)
        {
            countDownImages[i].gameObject.SetActive(true);

            yield return new WaitForSeconds(waitTime);

            countDownImages[i].gameObject.SetActive(false);
        }
        countDownImages[countDownImages.Length - 1].gameObject.SetActive(false);
        yield return null;
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
        musicunpaused();
        tempo.update = 1;
    }

}