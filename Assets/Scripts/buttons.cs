﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttons : MonoBehaviour
{
    public bool canBePressed;
    bool isOk;
    //public KeyCode KeyToPress;
    public KeyCode assignedkey;
    public string colorkey;

    public GameObject GoodEffect, NiceEffect, perfectEffect, missEffect;

    public UnityEngine.UI.Button button;
    private SpriteRenderer theSR;
    public Sprite DefaultImage;
    public Sprite pressedImage;

    private Transform currentArrow;
    private float updatetimer;
    private const float delayTime = 0.15f;
    public InputChange loadInput;
    
    // Start is called before the first frame update
    void Start()
    {
        assignedkey = loadInput.Getkeyfromdictionary(colorkey);

        if (button)
        {

        }
        theSR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        isOk = false;
        if (GameManager.IsGamePaused) return;

        updatetimer += Time.deltaTime;
        if (Input.GetKeyDown(assignedkey))
        {
            theSR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(assignedkey))
        {
            theSR.sprite = DefaultImage;
        }



        if (Input.GetKeyDown(assignedkey))
        {
            if (updatetimer > delayTime)
            {
                updatetimer = 0;
            }
            else
            {
                return;
            }

            if (currentArrow == null)
            {
                GameObject obj = Instantiate(missEffect, transform.position, missEffect.transform.rotation);
                obj.SetActive(true);
                GameManager.instance.NoteMissed();
                return;
            }

            if (canBePressed)
            {
                isOk = true;

                if ((Mathf.Abs(currentArrow.position.y) <= 5000000000000) && (Mathf.Abs(currentArrow.position.y) >= 0.45))
                {
                    Debug.Log("Good");
                    GameObject obj = Instantiate(GoodEffect, transform.position, GoodEffect.transform.rotation);
                    obj.SetActive(true);
                    GameManager.instance.Good();


                }
                else if ((Mathf.Abs(currentArrow.position.y) < 0.45) && (Mathf.Abs(currentArrow.position.y) >= 0.15))
                {
                    Debug.Log("Nice");
                    GameObject obj = Instantiate(NiceEffect, transform.position, NiceEffect.transform.rotation);
                    obj.SetActive(true);

                    GameManager.instance.nice();
                }
                else if ((Mathf.Abs(currentArrow.position.y) < 0.15) && (Mathf.Abs(currentArrow.position.y) >= -0.15))
                {
                    Debug.Log("Perfect");
                    GameObject obj = Instantiate(perfectEffect, transform.position, perfectEffect.transform.rotation);
                    obj.SetActive(true);

                    GameManager.instance.perfect();
                }
                else if ((Mathf.Abs(currentArrow.position.y) < -0.45) && (Mathf.Abs(currentArrow.position.y) >= -0.15))
                {
                    Debug.Log("Nice");
                    GameObject obj = Instantiate(NiceEffect, transform.position, NiceEffect.transform.rotation);
                    obj.SetActive(true);

                    GameManager.instance.nice();
                }
                if ((Mathf.Abs(currentArrow.position.y) < -5000000000000) && (Mathf.Abs(currentArrow.position.y) >= -0.45))
                {
                    Debug.Log("Good");

                    GameManager.instance.Good();
                }
                GameManager.instance.NoteHit();
                Destroy(currentArrow.gameObject);



            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "flechas")
        {
            canBePressed = true;
            currentArrow = other.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "flechas")
        {
            canBePressed = false;
            currentArrow = null;

            if (!isOk)
            {

                GameObject obj = Instantiate(missEffect, transform.position, missEffect.transform.rotation);
                obj.SetActive(true);
                GameManager.instance.NoteMissed();

            }
            Destroy(other.gameObject);
            
        }
       
    }
}
