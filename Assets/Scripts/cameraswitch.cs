﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraswitch : MonoBehaviour
{
    [System.Serializable]
    public struct GuideData
    {
        public Camera camera;
        public float WaitTime;
        
    }
    private float espera = 0f;
    public GuideData[] camera;

    int index = 0;
    private Coroutine coroutine;

    // Start is called before the first frame update
    void Start()
    {
        // instancia el momento en el que empiza el game manager
       // GameManager.instance.OnStartGame.AddListener(StartSwitch);

        for (int i = 0;i < camera.Length; i++)
        {
            camera[i].camera.gameObject.SetActive(false);

        }
        camera[0].camera.gameObject.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
       if(GameManager.IsGamePaused)
        {
            return;
        }
            espera += Time.deltaTime;

            if(espera >= camera[index].WaitTime)
            {
                espera = 0f;

                camera[index].camera.gameObject.SetActive(false);


                index = (index + 1) % camera.Length;


                camera[index].camera.gameObject.SetActive(true);

            }


    }
    IEnumerator CameraSwitch()
    {

        for (int i = 1; i < camera.Length; i++)
        {

            yield return new WaitForSeconds(camera[i].WaitTime);

            camera[i - 1].camera.gameObject.SetActive(false);

            camera[i].camera.gameObject.SetActive(true);
            
            
           
            
        }
    }

    void StartSwitch()
    {
        StartCoroutine(CameraSwitch());
        
    }
}


