﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Image fadeImage;
    public AudioSource PlayS;
    public AudioSource ExitS;
    public AudioSource SelectS;
    public GameObject Canvas;

    public void Play()
    {
        Event();
        GetComponent<MMPressButtonReaction>().Buttonclick();
        //SceneManager.LoadScene("MenuSelection");
        StartCoroutine(LoadYourAsyncScene("MenuSelection"));
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Options()
    {
        GetComponent<MMPressButtonReaction>().Buttonclick();
        //SceneManager.LoadScene("OptionsMenu");
        StartCoroutine(LoadYourAsyncScene("Options"));
    }


    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;

        float fadeIntensity = 0;
        Color col;
        fadeImage.gameObject.SetActive(true);
        while (fadeIntensity < 1)
        {
            fadeIntensity += 2f * Time.deltaTime;

            col = fadeImage.color;
            col.a = fadeIntensity;

            fadeImage.color = col;

            yield return null;
        }

        col = fadeImage.color;
        col.a = fadeIntensity;

        fadeImage.color = col;
        yield return new WaitForSeconds(0.02f);
        asyncLoad.allowSceneActivation = true;
    }

    void Event()
    {
        StartCoroutine("DoSomething");
    }

    IEnumerator DoSomething()
    {
        yield return new WaitForSeconds(1);
        Canvas.gameObject.SetActive(true);
    }

    public void clickedplay()
    {
        PlayS.Play();
    }

    public void clickedexit()
    {
        ExitS.Play();
    }

    public void clickedselect()
    {
        SelectS.Play();
    }
    public void clickedselectoff()
    {
        SelectS.Stop();
    }
}