﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public Image fadeImage;
    public AudioSource BackS;
    public AudioSource SelectS;

    public void Return()
    {
        //SceneManager.LoadScene("OptionsMenu");
        StartCoroutine(LoadYourAsyncScene("NewMenu"));
    }
    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;



        float fadeIntensity = 0;

        Color col;
        fadeImage.gameObject.SetActive(true);
        while (fadeIntensity < 1)
        {
            fadeIntensity += 2.5f * Time.deltaTime;

            col = fadeImage.color;
            col.a = fadeIntensity;

            fadeImage.color = col;

            yield return null;
        }

        col = fadeImage.color;
        col.a = fadeIntensity;

        fadeImage.color = col;
        yield return new WaitForSeconds(0.001f);
        asyncLoad.allowSceneActivation = true;
    }

    public void clickedBack()
    {
        BackS.Play();
    }

    public void Selected()
    {
        SelectS.Play();
    }

    public void SelectedOff()
    {
        SelectS.Stop();
    }
}

