﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //-
    public AudioSource ResumeS;
    public AudioSource ExitS;
    public AudioSource MissS;
    public GameObject canvasScore; //pantalla que incorpora la puntuacion
    public Text score;
    public Text multiplier;
    public float currentScore;
    public float multi = 1; //multiplicador actual que hay 
    public float multiPerNote = 0.1f; //actualmente todas las notas se multiplican por el mismo valor

    public float scorePerNote = 300;
    public float scorePerGoodNote = 100;
    public float scorePerNiceNote = 200;

    public AudioSource theMusic;
    public bool startPlaying; //determina cuando debe empezar el juego 

    public tempo theBS; //Encargado de controlar la velocidad con la que caen las flechas
    public Animator anim;

    public int totalHits = 0;
    public int totalGood = 0;
    public int totalNice = 0;
    public int totalPerfect = 0;
    public int totalMiss = 0;
    public int FirstStarCondition = 200000;
    public int SecondStarCondition = 700000;
    public int ThirdStarCondition = 1500000;

    //animaciones de los altavoces
    public Animation cinematic;
    public Animation cinematic1;
    public Animation cinematic2;
    public Animation cinematic3;

    //leer = get     /escribir = set
    public static bool IsGamePaused { get; private set; }
    [Header("Events")]
    public UnityEvent OnStartGame;
    public UnityEvent OnResumeGame;
    public UnityEvent OnPauseGame;

    public GameObject Buttons; //Pantalla de los botones

    public GameObject Canvas; // El canvas con Interface

    //Elementos Para la Pausa
    public Image fadeImage;
    public Image[] countDownImages;
    public GameObject pauseMenuUI;
    float waitTime = 1.0f;
    public Light myLight;
    public Light myLight1;
    public Light myLight2;
    public Light myLight3;
    public Light myLight4;
    Coroutine corutine;//referencia a la corutina del fade in, puede ser null
    

    public GameObject CanvasFinal;

    public Text SCORE;
    public Text Score;

    public Text totalhits;

    public Text totalgood;

    public Text totalnice;

    public Text totalperfect;

    public Text totalmiss;

    public GameObject estrella1;

    public GameObject estrella2;

    public GameObject estrella3;

    public AudioMixer audioMixer;

    public Slider volumeSlider;

    private bool updatebutton = true;


    void Awake()
    {



        if (!instance)
        {
            instance = this;

        }
        else if (instance != this)
        {
            Destroy(this);
            Debug.LogError("Error habia dos instancias de Game Manager! Solo puede haber una!");
        }
        IsGamePaused = true;
        myLight.enabled = false;
        myLight1.enabled = false;
        myLight2.enabled = false;
        myLight3.enabled = false;
        myLight4.enabled = false;
    }

    void Update()
    {

        StartGame();
        UpdateButtons();

        if (!theMusic.isPlaying && startPlaying && IsGamePaused == false)
        {
            EndSong();
        }


    }

    public void NoteHit()
    {
        //Debug.Log("Hit On Time");
        // currentScore = currentScore;
        multi += 0.1f;
        if (multi >= 5)
        {
            anim.SetBool("Combo1", true);
        }
        if (multi >= 10)
        {
            anim.SetBool("Combo2", true);
        }
        else
        {


        }
        multiplier.text = multi.ToString();

    }
    public void perfect()
    {
        currentScore += (scorePerNote * multi);

        totalHits++;
        totalPerfect++;

        //scorePerNote; 
        score.text = currentScore.ToString();
        //NoteHit();
    }
    public void Good()
    {
        currentScore += (scorePerGoodNote * multi);

        totalHits++;
        totalGood++;

        //scorePerGoodNote;
        score.text = currentScore.ToString();

        //NoteHit();
    }
    public void nice()
    {
        currentScore += (scorePerNiceNote * multi);

        totalHits++;
        totalNice++;
        score.text = currentScore.ToString();

        //scorePerNiceNote;

        //NoteHit();
    }
    public void NoteMissed()
    {
        MissS.Play();
        Debug.Log("Missed Note");
        totalMiss++;
        multi = 1;
        multiplier.text = multi.ToString();
        anim.SetTrigger("Fail");
        anim.SetBool(("Combo1"), false);
        anim.SetBool(("Combo2"), false);
    }

    public void StartGame()
    {
        if (!startPlaying)
        {
            anim.SetBool("StartUp", true);

            if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape))
            {
                tempo.update = 1;
                startPlaying = true;
                theBS.hasStarted = true;
                myLight.enabled = true;
                myLight1.enabled = true;
                myLight2.enabled = true;
                myLight3.enabled = true;
                myLight4.enabled = true;
                cinematic.Play();
                cinematic1.Play();
                cinematic2.Play();
                cinematic3.Play();
                theMusic.Play();

                IsGamePaused = false;
                OnStartGame.Invoke();

                anim.SetBool("StartUp", false);
            }
        }
    }

    /// <summary>
    /// Encargado de controlar las teclas del juego
    /// </summary>
    private void UpdateButtons()
    {
        if (!startPlaying || !updatebutton) return;
        if (Input.GetKeyDown(KeyCode.Escape)) //pausar el juego
        {
            if (IsGamePaused)
                Resume();
            else
                Pause();
        }
    }

    public void SetUpdateEscapeButton(bool isActive)
    {
        updatebutton = isActive;
    }

    /// <summary>
    /// El juego vuelve a estar ejecutandose
    /// </summary>
    public void Resume()
    {
        if (corutine == null)
        {
            anim.gameObject.GetComponent<Animator>().enabled = true;
            corutine = StartCoroutine(ResumeCountdown());


        }


    }
    /// <summary>
    /// Se pausa el juego
    /// </summary>
    public void Pause()
    {
        
        pauseMenuUI.SetActive(true);
        IsGamePaused = true;
        OnPauseGame.Invoke();
        tempo.update = 0;
        musicpaused();
        cinematic.Stop();
        cinematic1.Stop();
        cinematic2.Stop();
        cinematic3.Stop();
        OnPauseGame.Invoke();
        anim.gameObject.GetComponent<Animator>().enabled = false;
    }
    public void LoadMenu()
    {
        IsGamePaused = true;
        musicpaused();
        Debug.Log("Loading menu...");
        tempo.update = 1;
        StartCoroutine(LoadYourAsyncScene("MenuSelection"));
        tempo.update = 0;
    }
    void musicpaused()
    {

        theMusic.Pause();

    }
    void musicunpaused()
    {

        theMusic.UnPause();

    }
    //Carga de Escenario
    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;



        float fadeIntensity = 0;

        Color col;
        fadeImage.gameObject.SetActive(true);
        while (fadeIntensity < 1)
        {
            fadeIntensity += 2.5f * Time.deltaTime;

            col = fadeImage.color;
            col.a = fadeIntensity;

            fadeImage.color = col;

            yield return null;
        }

        col = fadeImage.color;
        col.a = fadeIntensity;

        fadeImage.color = col;
        yield return new WaitForSeconds(0.001f);
        asyncLoad.allowSceneActivation = true;
        tempo.update = 0;
    }
    //Cuenta atrás
    IEnumerator ResumeCountdown()
    {

        //desactivamos pantalla menu
        pauseMenuUI.SetActive(false);
        // ponemos imagen 0 

        for (int i = 0; i < 3; i++)
        {
            countDownImages[i].gameObject.SetActive(true);

            yield return new WaitForSeconds(waitTime);

            countDownImages[i].gameObject.SetActive(false);
        }

        countDownImages[countDownImages.Length - 1].gameObject.SetActive(false);
        yield return null;

        musicunpaused();
        tempo.update = 1;

        //dejamos de estar en pausa
        IsGamePaused = false;
        cinematic.Play();
        cinematic1.Play();
        cinematic2.Play();
        cinematic3.Play();
        OnResumeGame.Invoke();
        corutine = null;
    }
    void lightpause()
    {
        OnPauseGame.Invoke();
        myLight.enabled = false;
    }
    void lightplay()
    {
        OnPauseGame.Invoke();
        myLight.enabled = true;
    }

    public void clickedResume()
    {
        ResumeS.Play();
    }

    public void clickedExit()
    {
        ExitS.Play();
    }
    void EndSong()
    {
        anim.SetBool("IDLE", false);
        anim.SetBool("Fail", false);
        anim.SetBool(("Combo1"), false);
        anim.SetBool(("Combo2"), false);
        anim.SetBool("SongEnd", true);
        Buttons.SetActive(false);
        Canvas.SetActive(false);
        Debug.Log("aa");
        canvasScore.SetActive(false);
        CanvasFinal.SetActive(true);

        totalgood.text = "     " + totalGood;
        totalnice.text = "     " + totalNice;
        totalperfect.text = "     " + totalPerfect;
        totalmiss.text = "      " + totalMiss;
        totalhits.text = "      " + totalHits;
        Score.text = "    " + currentScore;
        score.text = ((SaveData)SaveData.currentData).score.ToString();
        SaveGame();

        if (currentScore >= FirstStarCondition)
        {
            estrella1.SetActive(true);
            if (currentScore >= SecondStarCondition)
            {
                estrella2.SetActive(true);
                if (currentScore >= ThirdStarCondition)
                {
                    estrella3.SetActive(true);
                }

            }


        }
        SaveGame();
    }
    public void OnLoadGame()
    {
        int SlotName = PlayerPrefs.GetInt(GameConstants.NeonNightSKey);
        SaveData.currentData = (SaveData)SerializationManager.Load(SlotName);
    }
    public void SaveGame()
    {
        int SlotName = PlayerPrefs.GetInt(GameConstants.NeonNightSKey);
        var save = (SaveData)SaveData.currentData;
        SerializationManager.Save(SlotName, save);
    }
}