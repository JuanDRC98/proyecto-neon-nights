﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAutoMove : MonoBehaviour
{
    public float step = 5;
    private void Update()
    {
    var cameraPosition = Camera.main.gameObject.transform.position;
    cameraPosition.x += step;
        Camera.main.gameObject.transform.position = cameraPosition;
    }
}
