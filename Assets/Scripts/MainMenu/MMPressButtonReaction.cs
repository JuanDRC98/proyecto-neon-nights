﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MMPressButtonReaction : MonoBehaviour
{
    [SerializeField] private Animator animpress;
    [SerializeField] private Animator animlogo;
    [SerializeField] private Animator animfade;
    public GameObject pressImage;
    public GameObject logo;
    public GameObject bgfade;
    public GameObject buttons;
    public GameObject Canvas;

    private void ButtonPress()
    {
        animpress.SetTrigger("Boipressingkey");
        StartCoroutine(WaitToDissappear());
    }

    public void Buttonclick()
    {
        animfade.SetTrigger("Anybuttontheboipressed");
    }

    IEnumerator WaitToDissappear()
    {
        yield return new WaitForSeconds(1);
        pressImage.gameObject.SetActive(false);
        bgfade.gameObject.SetActive(true);
        animlogo.SetTrigger("Boipressedkey");
        yield return new WaitForSeconds(1);
        buttons.gameObject.SetActive(true);
    }



void Start()
    {


        Canvas.gameObject.SetActive(false);
        //Loading.enabled = false;
    }

    //IEnumerator DoSomething()
    //{
    //    yield return new WaitForSeconds(1);
       
    //    ChibiHed.enabled = true;
    //    Loading.enabled = true;
    //}

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
            ButtonPress();
    }
}
