﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class MenuSelection : MonoBehaviour
{
    public AudioSource BackS;
    public AudioSource PlayS;
    // Loading image
    public Image fadeImage;
    public Image fade2Image;
    public Image Tuto;
    public Image ChibiHed;
    public Image Loading;
    // Rotator
    public GameObject rotator;
    // Key to scroll down
    public KeyCode KeyScrolldown;
    // Key to scroll up
    public KeyCode KeyScrollup;
    [SerializeField, Range(0.0f, 5.0f)] float duration = 0.5f;
    Vector3 currentposition;
    //[SerializeField] Chibi character;




    private void Start()
    {
        Tuto.enabled = false;
        ChibiHed.enabled = false;
        Loading.enabled = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyScrolldown))
        {
            currentposition += new Vector3(0, 0, 72.0f);
            //Do Rotate
            rotator.transform.DORotate(currentposition, duration, RotateMode.Fast);
            //character.DisplayCharacter(true);
        }
         if (Input.GetKeyDown(KeyScrollup))
        {
            currentposition -= new Vector3(0, 0, 72.0f);
            //Do Rotate
            rotator.transform.DORotate(currentposition, duration, RotateMode.Fast);
            //character.DisplayCharacter(false);
        }
        currentposition.z %= 360;

    }
    void Event()
    {
        StartCoroutine("DoSomething");
    }

    IEnumerator DoSomething()
        {
            yield return new WaitForSeconds(1);
            Tuto.enabled = true;
            ChibiHed.enabled = true;
            Loading.enabled = true;
    }
        


    public void Selected1()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("Juntitos"));
    }
    public void Selected2()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("ella"));
    }
    public void Selected3()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("ciudad de caramelos"));
    }
    public void Selected4()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("saltotemporal"));
    }
    public void Selected5()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("FelizDia"));
    }
    public void SelectedTut()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(LoadYourAsyncScene("Tutorial"));
    }
    public void Return()
    {
        //SceneManager.LoadScene("OptionsMenu");
        Event();
        StartCoroutine(Regreso("NewMenu"));
    }
    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;



        //float fadeIntensity = 0;

        //Color col;
        //fadeImage.gameObject.SetActive(true);
        //while (fadeIntensity < 1)
        //{
        //    fadeIntensity += 0.001f * Time.deltaTime;

        //    col = fadeImage.color;
        //    col.a = fadeIntensity;

        //    fadeImage.color = col;

        //    yield return null;
        //}

        //col = fadeImage.color;
        //col.a = fadeIntensity;

        //fadeImage.color = col;
        yield return new WaitForSeconds(7.3f);

      
            asyncLoad.allowSceneActivation = true;
        }
    IEnumerator Regreso(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;



        float fade2Intensity = 0;

        Color col;
        fade2Image.gameObject.SetActive(true);
        while (fade2Intensity < 1)
        {
            fade2Intensity += 2.5f * Time.deltaTime;

            col = fade2Image.color;
            col.a = fade2Intensity;

            fade2Image.color = col;

            yield return null;
        }

        col = fade2Image.color;
        col.a = fade2Intensity;

        fade2Image.color = col;
        yield return new WaitForSeconds(0.001f);
        asyncLoad.allowSceneActivation = true;
    }


    public void clickedexit()
    {
        BackS.Play();
    }

    public void clickedplay()
    {
        PlayS.Play();
    }
}
