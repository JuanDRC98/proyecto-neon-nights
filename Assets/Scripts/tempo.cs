﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tempo : MonoBehaviour
{
    public static float update = 1;
    public float arrowtempo;

    public bool hasStarted;

    
    // Start is called before the first frame update
    void Start()
    {
        arrowtempo = arrowtempo / 60f;
    }

    // Update is called once per frame
    void Update()
    {
        if(!hasStarted)
        {
           /* if (Input.anyKeyDown)
            {
                hasStarted = true;
            }*/
        }
        else
        {
            transform.position -= new Vector3(0f, arrowtempo * Time.deltaTime, 0f)*update;
        }
    }
}
