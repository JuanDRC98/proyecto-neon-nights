﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ani2 : MonoBehaviour
{
    [SerializeField] private Animator ani;

    
    public AudioSource CandyLand;
    //public void DisplayCharacter(bool hide)
    //{
    //chibi.SetBool("Sale", hide);
    //}

    private void OnTriggerEnter2D(Collider2D other)
    {
       
       if (other.CompareTag("Funciona2"))
        {
            
            ani.SetBool("Sale", true);
            CandyLand.Play();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Funciona2"))
        {
            ani.SetBool("Sale", false);
            CandyLand.Stop();
        }
    }

    public void clickanim()
    {
        ani.SetBool("selected", true);
    }

    public void clickanim2()
    {
        ani.SetBool("selected", false);
    }

    public void clicked()
    {
        ani.SetBool("click", true);
    }
}
