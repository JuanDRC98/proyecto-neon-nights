﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputChange : MonoBehaviour
{
    private Dictionary<string, KeyCode> inputDic = new Dictionary<string, KeyCode>();

    public Text greenText;
    public Text blueText;
    public Text purpleText;
    public Text yellowText;
    public string greenButton;
    public string blueButton;
    public string purpleButton;
    public string yellowButton;
    //public string assignedkey;
    public GameObject clickedbutton;


    // Start is called before the first frame update
    void Start()
    {
        inputDic.Add("Green", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Green", "D")));
        inputDic.Add("Blue", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Blue", "F")));
        inputDic.Add("Purple", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Purple", "J")));
        inputDic.Add("Yellow", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Yellow", "K")));

        if(greenText)
            greenText.text = inputDic["Green"].ToString();
        if (blueText)
            blueText.text = inputDic["Blue"].ToString();
        if (purpleText)
            purpleText.text = inputDic["Purple"].ToString();
        if (yellowText)
            yellowText.text = inputDic["Yellow"].ToString();
    }

    public KeyCode Getkeyfromdictionary(string key)
    {
        KeyCode Keyvalue;
        inputDic.TryGetValue(key, out Keyvalue);
        return Keyvalue;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(inputDic["Green"]))
            Debug.Log("Press");
    }
    private void OnGUI()
    {
        if (clickedbutton !=null)
        {
            clickedbutton.transform.GetChild(0).GetComponent<Text>().text = "...";
            Event e = Event.current;
            if (e.isKey)
            {
                inputDic[clickedbutton.name] = e.keyCode;
                clickedbutton.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                clickedbutton = null;
            }
        }
    }
    public void ChangeButton(GameObject clicked)
    {
        clickedbutton = clicked;
    }

    public void SaveKeys()
    {
        foreach (var key in inputDic)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }

        PlayerPrefs.Save();
    }

}
