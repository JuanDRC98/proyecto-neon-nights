﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartManager : MonoBehaviour
{
    // Start is called before the first frame update
     ScoreBoard score = new ScoreBoard();
    private void Start()
    {
        //has savegame?

    }
    public void NewGame(bool force)
    {
        if (score.points == int.MinValue)
        {
            //error message 
            Debug.LogWarning("There is no name for the slot, choose one before continue...");
            return;
        }

        if (!force && Directory.Exists(Application.persistentDataPath + "/saves/" + score.points + ".save")) //exist the slot
        {
            //popup message

            //return;
        }

        score.points = 0;
        SaveData data = new SaveData();
        data.score = score;
        SaveData.currentData = data;

        PlayerPrefs.SetInt("CurrentSlot", score.points);

        SerializationManager.Save(score.points, data);

    }
    public void SetNewScore(int scorename)
    {
        score.points = scorename;
    }
    public void ChangeLanguage(SystemLanguage language)
    {

    }
}