﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
[System.Serializable]
public class SaveSettings : SaveData
{
    public float VolumeSlider;

    //public SaveSettings();

}
/*Clase base de la que heredan todos los datos que se quieran guardar */
public class SaveDataSettings
{

    private static SaveData _currentData;
    //datos actuales de la partida. El GameManager debe ser el que inicialice estos datos y el que los guarde. Se puede realizar un Cast a clases personalizadas
    public static SaveData currentData;

    public Menusettings menusettings;

}

